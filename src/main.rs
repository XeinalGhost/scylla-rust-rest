#![feature(proc_macro_hygiene, decl_macro)]

mod storage;
mod controllers;

#[macro_use] pub extern crate rocket;
#[macro_use] pub extern crate cdrs;
#[macro_use] pub extern crate cdrs_helpers_derive;

use cdrs::authenticators::NoneAuthenticator;
use cdrs::cluster::session::{new as new_session, Session};
use cdrs::cluster::{ClusterTcpConfig, NodeTcpConfigBuilder, TcpConnectionPool};
use cdrs::load_balancing::{RoundRobin};
use crate::storage::{ScyllaStorage};
use std::env;
use config::*;

pub type CurrentSession = Session<RoundRobin<TcpConnectionPool<NoneAuthenticator>>>;

fn create_db_session(address: String) -> CurrentSession {
    let node = NodeTcpConfigBuilder::new(&address, NoneAuthenticator {}).build();
    let cluster_config = ClusterTcpConfig(vec![node]);
    new_session(&cluster_config, RoundRobin::new()).expect("Error creating db session")
}

fn main() {
    let mut config: Config = Config::default();
    config.merge(File::with_name(&env::var("CONFIG_PATH").expect("Missing CONFIG_PATH env")));

    let session: CurrentSession =
        create_db_session(config.get("storage.address").unwrap());
    let db: ScyllaStorage = ScyllaStorage::new(session);
    db.init();
    rocket::ignite()
        .manage(db)
        .mount("/book",
               routes![controllers::create, controllers::get, controllers::get_all, controllers::delete])
        .launch();
}
