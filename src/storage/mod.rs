use cdrs::query::*;
use crate::CurrentSession;
use cdrs::types::prelude::TryFromRow;
use cdrs::types::rows::Row;

pub trait Storage<T> {
    fn get(&self, id: &str) -> T;
    fn get_all(&self) -> Vec<T>;
    fn create(&self, item: T);
    fn delete(&self, id: &str);
}

pub trait QueryConvertible {
    fn into_query_values(&self) -> QueryValues;
    fn into_query_keys(&self) -> Vec<String>;
}

pub struct ScyllaStorage {
    db_session: CurrentSession
}

impl ScyllaStorage {
    pub fn new(session: CurrentSession) -> Self {
        ScyllaStorage { db_session: session }
    }

    pub fn init(&self) {
        self.create_keyspace();
        self.create_table();
    }

    fn create_keyspace(&self) {
        let create_ks: &'static str = "CREATE KEYSPACE IF NOT EXISTS test_ks WITH REPLICATION = { \
                                   'class' : 'SimpleStrategy', 'replication_factor' : 1 };";
        self.db_session.query(create_ks).expect("Keyspace creation error");
    }

    fn create_table(&self) {
        let create_table_cql =
            "CREATE TABLE IF NOT EXISTS test_ks.book (id text PRIMARY KEY, name text);";
        self.db_session
            .query(create_table_cql)
            .expect("Table creation error");
    }
}

impl <T: QueryConvertible + TryFromRow> Storage<T> for ScyllaStorage {
    fn get(&self, id: &str) -> T {
        let select_struct_cql = "SELECT * FROM test_ks.book WHERE id = ?";
        let row: Row = self.db_session
            .query_with_values(select_struct_cql, query_values!(id))
            .expect("Error on get")
            .get_body()
            .expect("Error on get body")
            .into_rows()
            .expect("Error converting to rows")
            .into_iter()
            .last()
            .expect("Not found");
        T::try_from_row(row).expect("Can't cast Row to object")
    }

    fn get_all(&self) -> Vec<T> {
        let select_struct_cql = "SELECT * FROM test_ks.book";
        self.db_session
            .query(select_struct_cql)
            .expect("Error on get all")
            .get_body()
            .expect("Error on get body")
            .into_rows()
            .expect("Error converting to rows")
            .into_iter()
            .map(|row: Row| T::try_from_row(row).expect("Can't cast Row to object"))
            .collect()
    }

    fn create(&self, item: T) {
        let insert_struct_cql =
            format!(
                "INSERT INTO test_ks.book ({}) VALUES ({})",
                item.into_query_keys().join(", "),
                item.into_query_keys()
                    .into_iter()
                    .map(|_| String::from("?"))
                    .collect::<Vec<String>>().join(", "));
        self.db_session
            .query_with_values(insert_struct_cql, item.into_query_values())
            .expect("Error on create");
    }

    fn delete(&self, id: &str) {
        let delete_struct_cql = "DELETE FROM test_ks.book WHERE id = ?";
        self.db_session
            .query_with_values(delete_struct_cql, query_values!(id))
            .expect("Error on delete");
    }
}