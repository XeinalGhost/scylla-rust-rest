use crate::storage::{Storage, QueryConvertible, ScyllaStorage};
use cdrs::query::{QueryValues};
use uuid::Uuid;
use rocket::State;
use rocket_contrib::json::{Json};
use serde::{Deserialize, Serialize};
use cdrs::types::prelude::TryFromRow;
use cdrs::types::from_cdrs::FromCDRSByName;

#[derive(Debug, Deserialize, Serialize, TryFromRow, Clone)]
pub struct Book {
    id: String,
    name: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SearchResponse<T> {
    items: Vec<T>,
    total: usize,
}

impl QueryConvertible for Book {
    fn into_query_values(&self) -> QueryValues {
        query_values!("id" => Uuid::new_v4().to_string(), "name" => *self.name)
    }

    fn into_query_keys(&self) -> Vec<String> {
        vec![String::from("id"), String::from("name")]
    }
}

#[get("/<id>")]
pub fn get(storage: State<ScyllaStorage>, id: String) -> Json<Book> {
    Json(storage.inner().get(&id))
}

#[delete("/<id>")]
pub fn delete(storage: State<ScyllaStorage>, id: String) {
    let book_stg: &dyn Storage<Book> = storage.inner();
    book_stg.delete(&id)
}

#[get("/")]
pub fn get_all(storage: State<ScyllaStorage>) -> Json<SearchResponse<Book>> {
    let books: Vec<Book> = storage.inner().get_all();
    let total: usize = (&books).len();
    Json(SearchResponse { items: books, total })
}

#[post("/", format = "application/json", data = "<book>")]
pub fn create(storage: State<ScyllaStorage>, book: Json<Book>) {
    storage.inner().create(book.into_inner());
}